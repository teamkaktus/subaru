<?php /*
Template Name: Главная
Template Post Type: page
 */
 ?>
<?php get_header();?>
<div class="main-top-banner">
  <div class="container">
    <span class="main-banner-title">
      Ремонт автомобилей Subaru</br>любой сложности
    </span>
    <div class="main-banner-btn " data-toggle="modal" data-target="#myModalHorizontal">
      <span class="banner-btn-title">
        Записаться на сервис
      </span>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-7 hidden-xs">
      <div class="slider-title">
        ТЕХЦЕНТР «SUBARU» 
      </div>
      <?php masterslider(1); ?>
    </div>
    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
      <div class="slider-title">
        ТЕХЦЕНТР «SUBARU» 
      </div>
      <?php masterslider(2); ?>
    </div>
    <div class="col-lg-4 main-scrol-container">
      <?php
        if (have_posts()):while (have_posts()):the_post(); 
          the_content(); 
        endwhile; else:
          __('Извините такой страницы не найдено!'); 
        endif;
      ?>
      </div>
    </div>
  </div>
</div>
<div class="main-midlle-container">
  <div class="container">
    <div class="row">
      <?php if( have_rows('what_we_doing') ): ?>
        <?php while( have_rows('what_we_doing') ): the_row(); ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 main-text-container">
            <div class="main-text-title">
              <img src="<?= the_sub_field('service_icon'); ?>" alt="<?= the_sub_field('service_name'); ?>"/>
              <span class="main-midle-text-title">
                <?= the_sub_field('service_name'); ?>
              </span>
            </div>
            <p class="middle-main-text">
              <?= get_sub_field('service_desciption'); ?>
            </p>
          </div>     
        <?php endwhile; ?>
      
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="container">
  <div class="main-promotions-title-container">
    <span class="main-promotions-title">
      АКЦИИ
    </span>
    <span class="main-promotions-all-link hidden-xs">
      <a href="/">Все акции</a>
    </span>
  </div>
  <div class="row">
    <?php if( have_rows('main_actions') ): ?>
      <?php while( have_rows('main_actions') ): the_row();  ?>
      <?php 
        $post = get_post( get_sub_field('main_action') );
        if ( has_post_thumbnail( $post->ID ) ) { ?>
          <div class="col-lg-6">
            <div class="promotions-img">
              <img src="<?= get_the_post_thumbnail_url( $post->ID, 'Actions' ) ?>" alt="" class="imd-promo">
              <div class="more-info">
                <a href="<?= get_permalink( $_post->ID ) ?>">Узнать больше <img src="/wp-content/themes/subaru/images/promo_arrow.png" class="promo_arrow"></a>  
              </div>
            </div>
            <div class="promotions-text-container">
              <span class="promotions-text-title">
                <?= $post->post_title; ?>
              </span>
              <p class="promotions-text">

                <?= the_field( 'short_text', $post->ID ); ?>
              </p>
            </div>
          </div>
    
        <?php }
        unset ($post);
      ?>
      <?php endwhile; ?>
    
    <?php endif; ?>
    <div class="promotions-all-link-mob-div hidden-lg hidden-md hidden-sm" >
      <span class="main-promotions-all-link ">
        <a href="/">Все акции</a>
      </span>
    </div>
  </div>
</div>
<div class="main-form-container">
    <div class="container">
      <div class="col-lg-6 col-md-6 col-sm-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
        <div class="main-form-title">
          Запись на сервис
        </div>
        <form action="" id="contact">
          <div class="row main-form-margin">
            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
              <input type="text" id="name" name="name" value="" class="" placeholder="Имя *"/>
              <span id="nameError" style="color: red; display: none;">Не коректно заполнено Имя</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
              <input type="text" name="company" value="" class="" placeholder="Компания"/>
            </div>
          </div>
          <div class="row main-form-margin">
            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
              <input type="text" id="phone" name="phone" value="" class="" placeholder="Телефон *"/>
              <span id="phoneError" style="color: red; display: none;">Не коректно введён телефон</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
              <label>
                <input name="rePhone" type="checkbox"  class="checkbox">
                <span class="checkbox-custom"></span>
                <span class="label">Получить ответ по телефону</span>
              </label>
            </div>
          </div>
          <div class="row main-form-margin">
            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
              <input type="text" id="email" name="email" value="" class="" placeholder="E-mail*"/>
              <span id="emailError" style="color: red; display: none;">Не коректно введён E-mail</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 main-select-style">
              <select name="category" class="">
                  <option disabled selected>Категория обращения</option>
                  <option>Замена масла</option>
                  <option>Продув инжектора</option>
                </select>
            </div>
          </div>
          <div class="row main-form-margin">
            <div class="col-lg-12 col-md-12 col-sm-12 main-textarea-style">
              <textarea class="" name="coment" placeholder="Введите комментарий"></textarea>
            </div>
          </div>
          <div class="row main-form-margin">
            <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-9 main-checkbox-style">
              <label>
                <input id="confidentional" name="confidentional" type="checkbox" class="checkbox-licenz">
                <span class="checkbox-custom-licenz"></span>
                <span class="label-licenz">Я согласен с <a href="/politika-konfidentsialnosti/" class="label-licenz-1">политикой конфиденциальности</a></span>
              </label>
                <span id="confidentionalError" style="color: red; display: none;">
                  Подтвердите согласие с политикой конфиденциальности
                </span>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-4 col-xs-6 main-form-btn send">
              <span>Отправить заявку</span>
            </div>
          </div>
        </form>
    </div>
  </div>  
</div>
<?php get_footer(); 