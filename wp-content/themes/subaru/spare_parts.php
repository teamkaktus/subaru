<?php /*
Template Name: Запчасти
Template Post Type: page
 */
 ?>
<?php get_header(); ?>  
<div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
    <div class="container">
        <?php if (function_exists('bcn_display')) {
            bcn_display();
        } ?>
    </div>
</div>
<section class="content-box works">
  <div class="container text-center">
    
    <span class="services-title shares-title"><?php the_title(); ?></span>
      <div class="row">
          <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
          <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
              <div class="marg_foto_share">
                  <img class="img_whight" src="<?= get_the_post_thumbnail_url(); ?>">
              </div>
              <div class="text_entry text_spare">
                <?php
                    if (have_posts()):while (have_posts()):the_post(); 
                        the_content(); 
                    endwhile; else:
                        __('Извините такой страницы не найдено!'); 
                    endif; 
                ?>
              </div>
              <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4">
              </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
      </div>
    </div>
  </div>
    <div class="main-form-container">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
                <div class="main-form-title spare_parts_title_form">
                    Узнать информацию о наличии и стоимости запчастей
                </div>
                <form action="" id="contact">
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="name" name="name" value="" class="" placeholder="Имя *"/>
                      <span id="nameError" style="color: red; display: none;">Не коректно заполнено Имя</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" name="company" value="" class="" placeholder="Компания"/>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="phone" name="phone" value="" class="" placeholder="Телефон *"/>
                      <span id="phoneError" style="color: red; display: none;">Не коректно введён телефон</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
                      <label>
                        <input name="rePhone" type="checkbox"  class="checkbox">
                        <span class="checkbox-custom"></span>
                        <span class="label">Получить ответ по телефону</span>
                      </label>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="email" name="email" value="" class="" placeholder="E-mail*"/>
                      <span id="emailError" style="color: red; display: none;">Не коректно введён E-mail</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-select-style">
                      <select name="category" class="">
                          <option disabled selected>Категория обращения</option>
                          <option>Замена масла</option>
                          <option>Продув инжектора</option>
                        </select>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-12 col-md-12 col-sm-12 main-textarea-style">
                      <textarea class="" name="coment" placeholder="Введите комментарий"></textarea>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-9 main-checkbox-style">
                      <label>
                        <input id="confidentional" name="confidentional" type="checkbox" class="checkbox-licenz">
                        <span class="checkbox-custom-licenz"></span>
                        <span class="label-licenz">Я согласен с <a href="/politika-konfidentsialnosti/" class="label-licenz-1">политикой конфиденциальности</a></span>
                      </label>
                        <span id="confidentionalError" style="color: red; display: none;">
                          Подтвердите согласие с политикой конфиденциальности
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-6 main-form-btn send">
                      <span>Отправить заявку</span>
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); 