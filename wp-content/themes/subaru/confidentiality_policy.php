<?php /*
Template Name: Политикой конфиденциальности
Template Post Type: page
 */
?>
<?php get_header(); ?>
    <div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
    <section class="content-box works">
        <div class="container text-center">
            <?php/*
      if (have_posts()):while (have_posts()):the_post(); 
        the_content(); 
      endwhile; else:
        __('Извините такой страницы не найдено!'); 
      endif; */?>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
                    <?php
                        if (have_posts()):while (have_posts()):the_post(); 
                            the_content(); 
                          endwhile; else:
                            __('Извините такой страницы не найдено!'); 
                          endif;
                    ?>
                </div>
            </div>
        </div>
        </div>
        <div class="main-form-container">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
                    <div class="main-form-title spare_parts_title_form">
                        Узнать информацию о наличии и стоимости запчастей
                    </div>
                    <form class="form_cont_1" action="">
                        <div class="row main-form-margin">
                            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                                <input type="text" name="" value="" class="" placeholder="Имя *"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                                <input type="text" name="" value="" class="" placeholder="Компания"/>
                            </div>
                        </div>
                        <div class="row main-form-margin">
                            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                                <input type="text" name="" value="" class="" placeholder="Телефон *"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
                                <label>
                                    <input type="checkbox" class="checkbox">
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Получить ответ по телефону</span>
                                </label>
                            </div>
                        </div>
                        <div class="row main-form-margin">
                            <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                                <input type="text" name="" value="" class="" placeholder="E-mail*"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 main-select-style">
                                <select class="">
                                    <option disabled selected>Категория обращения</option>
                                    <option>Замена масла</option>
                                    <option>Продув инжектора</option>
                                </select>
                            </div>
                        </div>
                        <div class="row main-form-margin">
                            <div class="col-lg-12 col-md-12 col-sm-12 main-textarea-style">
                                <textarea class="" placeholder="Введите комментарий"></textarea>
                            </div>
                        </div>
                        <div class="row main-form-margin">
                            <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-8 main-checkbox-style">
                                <label>
                                    <input type="checkbox" class="checkbox-licenz">
                                    <span class="checkbox-custom-licenz"></span>
                                    <span class="label-licenz">Я согласен с <a href="/" class="label-licenz-1">политикой конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-4 col-xs-4 main-form-btn">
                                <span>Отправить заявку</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); 