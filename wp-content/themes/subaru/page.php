<?php get_header(); ?>  
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
<section class="content-box works">
  <div class="container">
    <?php
      if (have_posts()):while (have_posts()):the_post(); 
        the_content(); 
      endwhile; else:
        __('Извините такой страницы не найдено!'); 
      endif; ?>
  </div>
</section>

<?php get_footer(); 