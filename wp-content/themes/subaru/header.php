<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/normalize.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/style.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/style_2.css">
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header>
    <div class="top-header-div">
      <div class="mobile-menu-text-container">
        <div class="mobile-menu-cose-container"></div>
        <div class="mobile-menu-style">
          <?php 
          wp_nav_menu( array(
            'menu_class'=>'menu',
              'theme_location'=>'top',
              'after'=>' /',
              //'theme_location'  => 'encorcapital'
          ) );
          ?>
        </div>
        <div class="mobile-menu-info-container">
          <span class="modile-menu-info">Россия, г. Москва, ул. Поморская, 48A</span>
          <span class="modile-menu-info">Телефон: +7 (495) 10-70-555</span>
          <span class="modile-menu-info " ><?=get_option('site_o_email');?><a class="modile-menu-info-link" href="mailto:<?=get_option('site_o_email');?>">wwwww</a></span>
        </div>  
        </div>
      <div class="container">
        <div class="mobile-menu-container hidden-lg hidden-md hidden-sm"></div>
        <div class="top-header-left-text hidden-xs">
          Москва, ул. Поморская, 48А
        </div>
        <div class="top-header-right-icon">
          <div class="">
            <div class="top-header-vk-icon">
              <a href="/"><img src="/wp-content/themes/subaru/images/vk_icon.png" alt="" class="top-header-vk-img"></a>
            </div>
            <div class="top-header-fb-icon">
              <a href="/"><img src="/wp-content/themes/subaru/images/fb_icon.png" alt="" class="top-header-fb-img"></a>
            </div>
            <div class="top-header-inst-icon">
              <a href="/"><img src="/wp-content/themes/subaru/images/inst_icon.png" alt="" class="top-header-inst-img"></a>
            </div>
            <div class="top-header-ok-icon">
              <a href="/"><img src="/wp-content/themes/subaru/images/ok_icon.png" alt="" class="top-header-ok-img"></a>
            </div>
            <div class="top-header-line-icon"></div>
            <div class="top-header-search-icon">
              <a href="/"><img src="/wp-content/themes/subaru/images/search_icon.png" alt="" class="top-header-search-img"></a>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="container">
      <div class="row header-margin">
        <div class="logo col-lg-2 col-md-2 col-sm-2 col-xs-5 xs-logo-width">
          <a href="/"><img src="<?=get_theme_mod('site_logo');?>" alt="<?=get_bloginfo('name');?>"></a>
        </div>
        <div class="wrap-menu header-menu-container col-lg-8 col-md-8 col-sm-8 hidden-xs">
          <?php 
          wp_nav_menu( array(
            'menu_class'=>'menu',
              'theme_location'=>'top',
              'after'=>' /',
              //'theme_location'  => 'encorcapital'
          ) );
          ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-7 text-right">
          <div class="header-phone">
            +7 (495) 10-70-555
          </div>
          <div class="header-link">
            <a style="cursor: pointer" data-toggle="modal" data-target="#myModalHorizontal">Записаться на сервис</a>
          </div>
        </div>  
      </div>
      <!--<div class="sandwich-menu hidden-lg">
          <span class="menu-global menu-top"></span>
          <span class="menu-global menu-middle"></span>
          <span class="menu-global menu-bottom"></span>
      </div>-->
      
      <div class="wrap-contact">
        <div class="site-email">
          <a href="mailto:<?=get_option('site_o_email');?>"><span><?=get_option('site_o_email');?></span></a>
        </div> 
        <div class="site-address">
          <?php
          if ($my_lang == 'ru') {
            echo get_option('site_o_address_ru');
          }
          elseif ($my_lang == 'en') {
            echo get_option('site_o_address_en'); 
          }
          ?>
        </div>
      </div>
      
    </div>
    <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content custom-modal-content">
            <!-- Modal Header -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">
              Запись на сервис
            </h4>
          </div>

            <!-- Modal Body -->
          <div class="modal-body">
            <form class="form-horizontal" role="form" id="headerContact">
              <div class="row main-form-margin">
                <div class="col-lg-6 col-md-6 col-sm-6 modal-main-input-style">
                  <input type="text" id="headerName" name="name" class="" placeholder="Имя *"/>
                  <span id="headerNameError" style="color: red; display: none;">Не коректно заполнено Имя</span>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 modal-main-input-style">
                  <input type="text" name="company" value="" class="" placeholder="Компания"/>
                </div>
              </div>
              <div class="row main-form-margin">
                <div class="col-lg-6 col-md-6 col-sm-6 modal-main-input-style">
                  <input type="text" id="headerPhone" name="phone" value="" class="" placeholder="Телефон *"/>
                  <span id="headerPhoneError" style="color: red; display: none;">Не коректно введён телефон</span>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
                  <label>
                    <input name="rePhone" type="checkbox" class="checkbox">
                    <span class="checkbox-custom"></span>
                    <span class="modal-label">Получить ответ по телефону</span>
                  </label>
                </div>
              </div>
              <div class="row main-form-margin">
                <div class="col-lg-6 col-md-6 col-sm-6 modal-main-input-style">
                  <input type="text" id="headerEmail" name="email" value="" class="" placeholder="E-mail*"/>
                  <span id="headerEmailError" style="color: red; display: none;">Не коректно введён E-mail</span>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 modal-main-select-style">
                    <select class="" name="category">
                      <option disabled selected>Категория обращения</option>
                      <option>Замена масла</option>
                      <option>Продув инжектора</option>
                    </select>
                </div>
              </div>
              <div class="row main-form-margin">
                <div class="col-lg-12 col-md-12 col-sm-12 modal-main-textarea-style">
                  <textarea name="coment" class="" placeholder="Введите комментарий"></textarea>
                </div>
              </div>
              <div class="row main-form-margin">
                  <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-12 main-checkbox-style">
                    <label>
                      <input type="checkbox" id="headerConfidentional" name="confidentional" class="checkbox-licenz">
                      <span class="modal-checkbox-custom-licenz"></span>
                      <span class="modal-label-licenz">Я согласен с <a href="/" class="mobile-label-licenz-1">политикой конфиденциальности</a></span>
                    </label>
                    <span id="headerConfidentionalError" style="color: red; display: none;">
                      Подтвердите согласие с политикой конфиденциальности
                    </span>
                  </div>
                  <div class="col-lg-4 col-md-5 col-sm-4 col-xs-6 col-xs-offset-3 modal-main-form-btn header-send">
                    <span>Отправить заявку</span>
                  </div>
                </div>
            </form>    
          </div>
        </div>
      </div>
    </div>
  </header>  
  <script>
    $(document).ready(function(){
      $('.mobile-menu-container').on('click', function() {
        $('.mobile-menu-text-container').show();	
      });
      $('.mobile-menu-cose-container').on('click', function() {
        $('.mobile-menu-text-container').hide();	
      });
    });
    $(document).ready(function(){
    $('.header-send').on('click', function (even) {
        if($('#headerName').val().length < 3){
          $('#headerNameError').show();
          $('#headerPhoneError').hide();
          $('#headerEmailError').hide();
          $('#headerConfidentionalError').hide();
        }else if($('#headerPhone').val().length < 3){
          $('#headerPhoneError').show();
          $('#headerNameError').hide();
          $('#headerEmailError').hide();
          $('#headerConfidentionalError').hide();
        }else if($('#headerEmail').val().length < 3){
          $('#headerEmailError').show();
          $('#headerNameError').hide();
          $('#headerPhoneError').hide();
          $('#headerConfidentionalError').hide();
        }else if(!$('#headerConfidentional').is(':checked')){
          $('#headerConfidentionalError').show();
          $('#headerNameError').hide();
          $('#headerPhoneError').hide();
          $('#headerEmailError').hide();
        }else{
          even.preventDefault();
            var res = $('#headerContact').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: '/wp-content/themes/subaru/mail.php',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#myModalHorizontal').modal('hide');  
            swal(
                'Повідомлення відправлено!',
                '',
                'success'
            );
            $('#contact')[0].reset();
            $('#headerNameError').hide();
            $('#headerPhoneError').hide();
            $('#headerEmailError').hide();
            $('#headerConfidentionalError').hide();
            $('#headerName').val('');
            $('#headerCompany').val('');
            $('#headerPhone').val('');
            $('#headerEmail').val('');
            $('#headerComent').val('');
        }  
    });
});
  </script>