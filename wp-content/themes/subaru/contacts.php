<?php /*
Template Name: Контакти
Template Post Type: page
 */
 ?>
<?php get_header(); ?>  
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
<section class="content-box works">
  <div class="container " style="position: relative;">
    <?php/*
      if (have_posts()):while (have_posts()):the_post(); 
        the_content(); 
      endwhile; else:
        __('Извините такой страницы не найдено!'); 
      endif; */?>
    <span class="services-title contacts_titles">КОНТАКТЫ</span>
      <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="adress_cont">Адрес, телефон и e-mail</div>
              <div class="text_cont">
                  <p>Адрес: Россия, г. Москва, ул. Поморская, 48А</p>
                  <p>Телефон: +7 (495) 10-70-555 (многоканальный)</p>
                  <p class="p_tel">+7 (495) 514-31-07</p>
                  <p class="p_tel">+7 (495) 514-74-87</p>
                  <p>E-mail: <span class="span_email">subaru@suba.ru</span></p>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="adress_cont">Время работы</div>
              <div class="text_cont">
                  <p>С понедельника по пятницу с 10:00 до 19:00;</p>
                  <p>в субботу с 10:00 до 17:30;</p>
                  <p>воскресенье — выходной день</p>
              </div>
          </div>
        <div class="col-xs-12 map-div-style" id="map"></div>
      </div>
  </div>
  <div class="main-form-container">
        <div class="container" >
          
            <div class="col-lg-6 col-md-6 col-sm-10 col-sm-offset-1">
                <div class="main-form-title">
                    Запись на сервис
                </div>
                <form action="" id="contact">
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="name" name="name" value="" class="" placeholder="Имя *"/>
                      <span id="nameError" style="color: red; display: none;">Не коректно заполнено Имя</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" name="company" value="" class="" placeholder="Компания"/>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="phone" name="phone" value="" class="" placeholder="Телефон *"/>
                      <span id="phoneError" style="color: red; display: none;">Не коректно введён телефон</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
                      <label>
                        <input name="rePhone" type="checkbox"  class="checkbox">
                        <span class="checkbox-custom"></span>
                        <span class="label">Получить ответ по телефону</span>
                      </label>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="email" name="email" value="" class="" placeholder="E-mail*"/>
                      <span id="emailError" style="color: red; display: none;">Не коректно введён E-mail</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-select-style">
                      <select name="category" class="">
                          <option disabled selected>Категория обращения</option>
                          <option>Замена масла</option>
                          <option>Продув инжектора</option>
                        </select>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-12 col-md-12 col-sm-12 main-textarea-style">
                      <textarea class="" name="coment" placeholder="Введите комментарий"></textarea>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-9 main-checkbox-style">
                      <label>
                        <input id="confidentional" name="confidentional" type="checkbox" class="checkbox-licenz">
                        <span class="checkbox-custom-licenz"></span>
                        <span class="label-licenz">Я согласен с <a href="/politika-konfidentsialnosti/" class="label-licenz-1">политикой конфиденциальности</a></span>
                      </label>
                        <span id="confidentionalError" style="color: red; display: none;">
                          Подтвердите согласие с политикой конфиденциальности
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-6 main-form-btn send">
                      <span>Отправить заявку</span>
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); 