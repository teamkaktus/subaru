<?php /*
Template Name: Услуги
Template Post Type: page
 */
?>
<?php get_header(); ?>
<div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
    <section class="content-box works">
        <div class="container text-center">
            <div class="row">
                <span class="services-title">УСЛУГИ</span>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <?php
                $params = array(
                    'cat' => 3,
                    'posts_per_page' => 0,
                    'post_type' => 'post'
                );
                query_posts($params);
                $wp_query->is_archive = false;
                $wp_query->is_home = false;

                while (have_posts()): the_post(); ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-left service-main-container">
                        <div class="services-container">
                            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'list_image') ?>">
                            <div class="more-info-serv">
                                <a href="<?= get_permalink(get_the_ID()) ?>">Узнать больше<img
                                            src="/wp-content/themes/subaru/images/promo_arrow.png" class="promo_arrow"></a>
                            </div>
                        </div>
                        <div class="promotions-text-container">
              <span class="promotions-text-title">
                <?php the_title(); ?>
              </span>
                            <p class="promotions-text">
                                <?= the_field('short_text', get_the_ID()); ?>
                            </p>
                        </div>
                    </div>
                <?php endwhile; ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-left last-services-container">
                    <div class="img-1">
                        <img src="/wp-content/themes/subaru/images/service_15.png">
                    </div>
                    <div class="nissan-logo-container">
                        <img class="img-2" src="/wp-content/themes/subaru/images/nisan_logo.png">
                    </div>
                    <!--<div class="more-info-serv">
                      <a href="/">Узнать больше<img src="/wp-content/themes/subaru/images/promo_arrow.png" class="promo_arrow"></a>
                    </div>-->
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); 