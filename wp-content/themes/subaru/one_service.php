<?php 
  /*
  Template Name: Одна услуга
  Template Post Type: post
  */
 ?>
<?php get_header(); ?>
    <div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
        <div class="container">
            <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </div>
    </div>
    <section class="content-box works">
        <div class="container">
            <div class="row top-one-serv-border">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
        <span class="back-to-all-serv">
          <a href="/uslugi/"><img src="/wp-content/themes/subaru/images/one_serv_arrow.png">Все услуги</a>
        </span>
                    <img class="t-o-img" src="<?= get_the_post_thumbnail_url() ?>">
                </div>
                <div class="col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 col-sm-7 col-xs-12 one-service-text">
                    <div class="one-service-title"> <?php the_title(); ?> </div>
                    <?php
                    if (have_posts()):while (have_posts()):the_post();
                        the_content();
                    endwhile;
                    else:
                        __('Извините такой страницы не найдено!');
                    endif; ?>
                </div>
                <dov class="border-one-serv-bottom"></dov>
            </div>
            <div class="row">
                <span class="all-t-o">Перечень плановых работ:</span>
                <?php if (have_rows('list_of_works')): ?>
                    <?php while (have_rows('list_of_works')): the_row();
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-sx-12 plus-t-o-1" data-toggle="modal"
                             data-target="#Modal<?= get_row_index() ?>">
                            <span><?php the_sub_field('work_name'); ?></span>
                        </div>
                        <!-- Modal -->
                        <div id="Modal<?= get_row_index() ?>" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header modal_head">
                                        <button class="close close_bott_x" type="button" data-dismiss="modal"><img
                                                    src="/wp-content/themes/subaru/images/close_btn.png"></button>
                                        <div class="modalTitle">
                                            <div class="adress_serv_title"><?php the_sub_field('work_name'); ?></div>
                                        </div>
                                    </div>
                                    <div class="modal-body mad_body_desc">
                                        <p><?php the_sub_field('work_description'); ?></p></div>
                                    <div class="modal-footer mod_footer_serv">
                                        <div class="more_shares more_bot_serv" >
                                            <a onclick="hidediv('Modal<?= get_row_index() ?>');" href="#myDiv">Записаться
                                                на сервис</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div
        <div style="display: none">
            
        </div>
        <div class="main-form-container"><a name="myDiv"></a>
            <div class="container">
              <div class="col-lg-6 col-md-6 col-sm-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
                <div class="main-form-title">
                  Запись на сервис
                </div>
                <form action="" id="contact">
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="name" name="name" value="" class="" placeholder="Имя *"/>
                      <span id="nameError" style="color: red; display: none;">Не коректно заполнено Имя</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" name="company" value="" class="" placeholder="Компания"/>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="phone" name="phone" value="" class="" placeholder="Телефон *"/>
                      <span id="phoneError" style="color: red; display: none;">Не коректно введён телефон</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-checkbox-style">
                      <label>
                        <input name="rePhone" type="checkbox"  class="checkbox">
                        <span class="checkbox-custom"></span>
                        <span class="label">Получить ответ по телефону</span>
                      </label>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-6 col-md-6 col-sm-6 main-input-style">
                      <input type="text" id="email" name="email" value="" class="" placeholder="E-mail*"/>
                      <span id="emailError" style="color: red; display: none;">Не коректно введён E-mail</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 main-select-style">
                      <select name="category" class="">
                          <option disabled selected>Категория обращения</option>
                          <option>Замена масла</option>
                          <option>Продув инжектора</option>
                        </select>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-12 col-md-12 col-sm-12 main-textarea-style">
                      <textarea class="" name="coment" placeholder="Введите комментарий"></textarea>
                    </div>
                  </div>
                  <div class="row main-form-margin">
                    <div class="col-lg-8 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-8 col-xs-9 main-checkbox-style">
                      <label>
                        <input id="confidentional" name="confidentional" type="checkbox" class="checkbox-licenz">
                        <span class="checkbox-custom-licenz"></span>
                        <span class="label-licenz">Я согласен с <a href="/politika-konfidentsialnosti/" class="label-licenz-1">политикой конфиденциальности</a></span>
                      </label>
                        <span id="confidentionalError" style="color: red; display: none;">
                          Подтвердите согласие с политикой конфиденциальности
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-6 main-form-btn send">
                      <span>Отправить заявку</span>
                    </div>
                  </div>
                </form>
            </div>
          </div>
        </div>
    </section>
<script>
    function hidediv(pam) {
      $('.modal-backdrop').css('display', 'none')
        if (document.getElementById(pam).style.display == "") {
            document.getElementById(pam).style.display = "none";
        }
        else {
            document.getElementById(pam).style.display = "";
        }
    }
</script>
<?php get_footer(); 
