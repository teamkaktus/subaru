$(document).ready(function(){
      $('.send').on('click', function (even) {
        if($('#name').val().length < 3){
          $('#nameError').show();
          $('#phoneError').hide();
          $('#emailError').hide();
          $('#confidentionalError').hide();
        }else if($('#phone').val().length < 3){
          $('#phoneError').show();
          $('#nameError').hide();
          $('#emailError').hide();
          $('#confidentionalError').hide();
        }else if($('#email').val().length < 3){
          $('#emailError').show();
          $('#nameError').hide();
          $('#phoneError').hide();
          $('#confidentionalError').hide();
        }else if(!$('#confidentional').is(':checked')){
          $('#confidentionalError').show();
          $('#nameError').hide();
          $('#phoneError').hide();
          $('#emailError').hide();
        }else{
          even.preventDefault();
            var res = $('#contact').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: '/wp-content/themes/subaru/mail.php',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });

            swal(
                'Повідомлення відправлено!',
                '',
                'success'
            );
            $('#contact')[0].reset();
            $('#nameError').hide();
            $('#phoneError').hide();
            $('#emailError').hide();
            $('#confidentionalError').hide();
            $('#name').val('');
            $('#company').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#coment').val('');
        }  
    });
    
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map("map", {
            center: [55.870157, 37.574118],
            zoom: 17,
            controls: []
        });

        myPlacemark = new ymaps.Placemark([55.870173, 37.573738], {
            hintContent: 'Собственный значок метки',
            balloonContent: 'Это красивая метка'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/wp-content/themes/subaru/js/loc_icon.png',
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        }),

        myMap.geoObjects
            .add(myPlacemark)
    }
});

