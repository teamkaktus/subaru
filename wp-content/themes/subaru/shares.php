<?php /*
Template Name: Акции
Template Post Type: page
 */
 ?>
<?php get_header(); ?>  
<div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
<section class="content-box works">
  <div class="container text-center">
    <span class="services-title shares-title">Акции</span>
    <div class="row">


      <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $params = array(
            'cat' => 4,
            'posts_per_page' => 2,
            'post_type' => 'post',
            'paged' => $paged
        );
        query_posts($params);
        $wp_query->is_archive = false;
        $wp_query->is_home = false;

        while (have_posts()): the_post(); ?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left ">
            <div class="services-container">
              <img class="img_share" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'list_image') ?>">
              <div class="more-info-serv more-info-shares">
                <a href="<?= get_permalink(get_the_ID()) ?>">Узнать больше<img src="/wp-content/themes/subaru/images/promo_arrow.png" class="promo_arrow"></a>
              </div>
            </div>
            <div class="promotions-text-container shares_title_2">
              <span class="promotions-text-title shares_title_span">
                <?php the_title(); ?>
              </span>
              <p class="promotions-text">
                <?= the_field('short_text', get_the_ID()); ?>
              </p>
            </div>
          </div>
        <?php endwhile;?>
      <?php wp_pagenavi(); ?>
    </div>
  </div>
</section>

<?php get_footer(); 