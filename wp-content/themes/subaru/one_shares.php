<?php /*
Template Name: Одна акція
Template Post Type: post
 */
 ?>
<?php get_header(); ?>  
<div class="breadcrumbs breadcrumbs-comtainer-style" typeof="BreadcrumbList" vocab="https://schema.org/">
  <div class="container">
      <?php if(function_exists('bcn_display'))
      {
          bcn_display();
  }?>
  </div>
</div>
<section class="content-box works">
  <div class="container">
      <div class="row text-center">
          <span class="services-title shares-title shares_tit"> <?php the_title(); ?> </span>
      </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
            <div class="date_shares">
            <?php the_field('date_created'); ?>
                </div>
            <div class="marg_foto_share">
                <img class="img_whight" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'list_image') ?>">
            </div>
            <div class="text_entry">
              <?php
              if (have_posts()):while (have_posts()):the_post();
                the_content();
              endwhile; else:
                __('Извините такой страницы не найдено!');
              endif; ?>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 more_shares">
                <a href="/aktsii/">Все акции</a>
            </div>
      </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
    </div>
  </div>
</section>
<?php get_footer(); 