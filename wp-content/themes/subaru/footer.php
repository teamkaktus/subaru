<footer>
  <div class="main-footer-conteiner">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-sm-push-6 no-padding text-right footer-first-mob-div col-xs-12 ">
          <div class="wrap-menu footer-menu-container">
            <?php 
            wp_nav_menu( array(
              'menu_class'=>'menu',
                'theme_location'=>'top',
                'after'=>' /',
                //'theme_location'  => 'encorcapital'
            ) );
            ?>
          </div>
          <div class="footer-social-icon">
            <a href="/"><img src="/wp-content/themes/subaru/images/footer_vk.png" alt="" class="footer-img-margin"></a>
            <a href="/"><img src="/wp-content/themes/subaru/images/footer_fb.png" alt="" class="footer-img-margin"></a>
            <a href="/"><img src="/wp-content/themes/subaru/images/footer_insta.png" alt="" class="footer-img-margin"></a>
            <a href="/"><img src="/wp-content/themes/subaru/images/footer_ok.png" alt="" class=""></a>
          </div>
          <span class="footer-date-span hidden-xs">
            © Субару Сервис, 1998-2017 
          </span></br>
          <span class="footer-date-span hidden-xs">
            Все права защищены.
          </span>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-sm-pull-6 col-xs-12 ">
          <span class="footer-title-1">
            Адрес, телефон и email
          </span>
          <span class="footer-adress">
            Россия, г. Москва, ул. Поморская, 48A
          </span>
          <span class="footer-phone">
            Телефон:<a href="tel:+7 (495) 10-70-555">+7 (495) 10-70-555</a>
          </span>
          <span class="footer-email">
            Email:<a href="mailto:subaru@suba.ru">subaru@suba.ru</a>
          </span>
          <span class="footer-reklama hidden-xs">
            Сделано в Рекламотерапия
          </span>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-sm-pull-6 col-xs-12 no-padding footer-last-div-mob-pad">
          <span class="footer-title-1">
            Время работы
          </span>
          <span class="footer-adress">
            С понедельника по пятницу с 10:00 до 19:30;
          </span>
          <span class="footer-phone">
            в субботу с 10:00 до 17:30;
          </span>
          <span class="footer-email">
            воскресенье — выходной день
          </span>
          <span class="footer-reklama">
            Политика конфиденциальности
          </span>
          <span class="footer-date-span hidden-lg hidden-md hidden-sm">
            © Субару Сервис, 1998-2017 
          </span></br>
          <span class="footer-date-span hidden-lg hidden-md hidden-sm">
            Все права защищены.
          </span>
          <span class="footer-reklama-mob hidden-lg hidden-md hidden-sm">
            Сделано в Рекламотерапия
          </span>
        </div>
          
      </div>
    </div>  
  </div>
</footer>
<?php wp_footer(); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>